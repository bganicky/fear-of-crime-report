f.d <- select(d, F.1:F.4)
f.names <- c(
	"Spokojenost s prací PČR",
	"Důvěra v PČR",
	"Spokojenost s Městskou policií",
	"Důvěra v Městskou policii"
)
f.cap <- c("Tabulka: Percepce činnosti bezpečnostních složek obecně")

# F.1
f.1.d <- select(d, SEX, COHORT, F.1)
f.1.d <- f.1.d[complete.cases(f.1.d$F.1), ]
f.1.cap = c("Tabulka: Spokojenost s prací PČR dle pohlaví")
f.1.sex <- f.1.d %>%
	group_by(SEX) %>%
	summarise(mean=mean(F.1), std=sd(F.1), min=min(F.1), max=max(F.1))
f.1.age <- f.1.d %>%
	group_by(COHORT) %>%
	summarise(mean=mean(F.1), std=sd(F.1), min=min(F.1), max=max(F.1))
f.1.both <- f.1.d %>%
	group_by(SEX, COHORT) %>%
	summarise(mean=mean(F.1), std=sd(F.1), min=min(F.1), max=max(F.1))

# F.2
f.2.d <- select(d, SEX, COHORT, F.2)
f.2.d <- f.2.d[complete.cases(f.2.d$F.2), ]
f.2.cap <- c("Tabulka: Důvěra v PČR dle pohlaví")
f.2.sex <- f.2.d %>%
	group_by(SEX) %>%
	summarise(mean=mean(F.2), std=sd(F.2), min=min(F.2), max=max(F.2))
f.2.age <- f.2.d %>%
	group_by(COHORT) %>%
	summarise(mean=mean(F.2), std=sd(F.2), min=min(F.2), max=max(F.2))
f.2.both <- f.2.d %>%
	group_by(SEX, COHORT) %>%
	summarise(mean=mean(F.2), std=sd(F.2), min=min(F.2), max=max(F.2))
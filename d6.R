# Data
d.6.d <- select(d, D.6_1:D.6_12)

# Human names
d.6.names <- c(
  "Rušení nočního klidu",
  "Volné pobíhání psů, neodklizení psích exkrementů",
  "Znečistění veřejného prostranství",
  "Neoprávněné zakládání skládek",
  "Vandalismus",
  "Sprejerství",
  "Vzbuzování veřejného pohoršení (např. pití alkoholu na veřejnosti)",
  "Žebrání",
  "Porušování dopravních předpisů",
  "Prodej alkoholu a cigaret mladistvým",
  "Protiprávní jednání směřující proti občanskému soužití (ublížení na cti, pomluva)",
  "Přestupky proti majetku (drobné krádeže)"
)
d.6.names.short <- c(
	"Noční klid",
	"Psi a exkrementy",
	"Znečistění veř. prost.",
	"Skládky",
	"Vandalismus",
	"Sprejerství",
	"Veř. pohoršení",
	"Žebrání",
	"Dopr. předpisy",
	"Prodej alk. a cig. mladistvým",
	"...ublížení na cti, pomluva",
	"Majet. přestupky (drobné krádeže)"
)

# Captions
d.6.table.cap <- c("Tabulka: Strach z jednotlivých kategorií trestných činů")
d.6.bplot.cap <- c("Graf: Strach z jednotlivých kategorií trestných činů")
d.6.lplot.cap <- c("Graf: Strach z jednotlivých kategorií trestných činů")